﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Hl7Searcher.Models;
using Microsoft.WindowsAPICodePack.Dialogs;
using Hl7Searcher.Helper;

namespace Hl7Searcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public string filesPathFolder;
        public List<string> filterdPathList = new List<string>();
        public Search search = new Search();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var dialog1 = new CommonOpenFileDialog();
            dialog1.IsFolderPicker = true;
            //dialog1.DefaultDirectory = @"E:\HL7Files\ParsedORUFiles";
            CommonFileDialogResult result = dialog1.ShowDialog();
            if (result.ToString().ToUpper() == "OK" && !string.IsNullOrWhiteSpace(dialog1.FileName))
            {
                filesPathFolder = dialog1.FileName;
                folder_path_tb.Text = "File Path: " + filesPathFolder;
                total_files_tb.Text = "Total Files: " + Directory.GetFiles(filesPathFolder).Count();
            }
            //var dialog = new System.Windows.Forms.FolderBrowserDialog();
            //DialogResult result = dialog.ShowDialog();
            //if (result.ToString().ToUpper() == "OK" && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
            //{
            //    filesPathFolder = dialog.SelectedPath;
            //    //System.Windows.Forms.MessageBox.Show(filesPathList.Count() + " files in folder");
            //    folder_path_tb.Text = "File Path: " + dialog.SelectedPath;
            //    total_files_tb.Text = "Total Files: " + Directory.GetFiles(dialog.SelectedPath).Count();
            //}
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            setAllSearchFields();
            searchHL7Files();
        }

        private void searchHL7Files()
        {
            filterdPathList.Clear();
            if (filesPathFolder != null)
            {
                int counter = 0;
                int totalFiles = Directory.GetFiles(filesPathFolder).Count();
                List<string> fileTextLines = new List<string>();
                String fileType;
                searching_count_tb.Text = "Search Started ... ";
                foreach (string filePath in Directory.GetFiles(filesPathFolder).ToList())
                {
                    if (File.Exists(filePath))
                    {
                        //filtering file received date
                        if (!FilterOnFileReceievedDate(filePath))
                        {
                            continue;
                        }

                        fileTextLines.Clear();
                        try
                        {
                            fileType = File.ReadLines(filePath).First().ToUpper().Split('|')[8].ToUpper();
                        } catch(Exception) {continue;/*error in reading file type*/}
                        
                        //filtering file type
                        if (search.fileTypes == null ||search.fileTypes.Count == 0 || search.fileTypes.Any(fileType.Contains))
                        {
                            fileTextLines = File.ReadAllLines(filePath, Encoding.UTF8).ToList();

                            //filtering exam performed date
                            if (!FilterOnExamPerformedDate(fileTextLines))
                            {
                                continue;
                            }
                            //filtering segmants
                            if (search.segments!=null && search.segments.Count > 0)
                            {
                                fileTextLines = fileTextLines.Where(x => search.segments.Any(x.Split('|')[0].ToUpper().StartsWith)).ToList();
                            }
                            //filtering keywords
                            if (search.keywords!=null && search.keywords.Count() > 0)
                            {
                                //filtering fields
                                if (search.fields != null && search.fields.Count() > 0)
                                {
                                    List<String> filteredLines = new List<string>();
                                    foreach (string num in search.fields)
                                    {
                                        int fieldNo = int.Parse(num.Split('.')[0]);
                                        filteredLines.AddRange(fileTextLines.Where(x => search.keywords.Any(x.Split('|')[fieldNo].Contains)).ToList());
                                    }
                                    fileTextLines = filteredLines;
                                }
                                else
                                {
                                    fileTextLines = fileTextLines.Where(x => search.keywords.Any(x.Contains)).ToList();
                                }
                            }
                        }
                        if (fileTextLines.Count() > 0)
                        {
                            filterdPathList.Add(filePath);
                        }
                        counter++;
                        searching_count_tb.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, (Action)(() => { searching_count_tb.Text = "Searching " + counter.ToString() + " ( "+((100 * counter) / totalFiles) +"%)"; filtered_files_count_tb.Text = "Total Filtered Files Count: " + filterdPathList.Count(); }));
                    }
                }
                filtered_files_count_tb.Text = "Total Filtered Files Count: " + filterdPathList.Count();
                searching_count_tb.Text = "Searched Completed";
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Please select folder to search in");
            }
        }

        private void setAllSearchFields()
        {
            search = new Search();
            if (!string.IsNullOrWhiteSpace(search_keywords_tb.Text))
            {
                search.keywords = search_keywords_tb.Text.ToUpper().Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(search_file_types_tb.Text))
            {
                search.fileTypes = search_file_types_tb.Text.ToUpper().Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(search_segments_tb.Text))
            {
                search.segments = search_segments_tb.Text.ToUpper().Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(search_fields_tb.Text))
            {
                search.fields = search_fields_tb.Text.ToUpper().Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            }
            if (!string.IsNullOrWhiteSpace(date_performed_start.Text))
            {
                search.datePerformedStart = date_performed_start.Text.TryParse();
            }
            if (!string.IsNullOrWhiteSpace(date_performed_end.Text))
            {
                search.datePerformedEnd = date_performed_end.Text.TryParse();
            }
            if (!string.IsNullOrWhiteSpace(date_received_start.Text))
            {
                search.dateReceivedStart = date_received_start.Text.TryParse();
            }
            if (!string.IsNullOrWhiteSpace(date_received_end.Text))
            {
                search.dateReceivedEnd = date_received_end.Text.TryParse();
            }
        }
        private Boolean FilterOnFileReceievedDate(string filePath)
        {
            DateTime fileReceivedDate = System.IO.File.GetCreationTime(filePath);
            if (!string.IsNullOrWhiteSpace(date_received_start.Text) || !string.IsNullOrWhiteSpace(date_received_end.Text))
            {
               // DateTime? dateReceivedStart = date_received_start.Text.TryParse();
                //DateTime? dateReceivedEnd = date_received_end.Text.TryParse();
                if ((search.dateReceivedStart.HasValue && search.dateReceivedStart.Value > fileReceivedDate) || (search.dateReceivedEnd.HasValue && search.dateReceivedEnd.Value.AddDays(1) < fileReceivedDate))
                {
                    return false;
                }
            }
            return true;

        }

        private Boolean FilterOnExamPerformedDate(List<string> fileTextLines)
        {
            if (search.datePerformedStart.HasValue || search.datePerformedEnd.HasValue)
            {
                string obrSegment = fileTextLines.Where(x => x.Split('|')[0].ToUpper().StartsWith("OBR")).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(obrSegment))
                {
                    DateTime? examPerformedDate = null;
                    List<string> obrSegmentArray = obrSegment.Split('|').ToList();
                    if (!string.IsNullOrWhiteSpace(obrSegmentArray.ElementAtOrDefault(7)))
                    {
                        examPerformedDate = obrSegmentArray.ElementAt(7).ToFormattedDateTime();
                    }
                    else if (!string.IsNullOrWhiteSpace(obrSegmentArray.ElementAtOrDefault(8)))
                    {
                        examPerformedDate = obrSegmentArray.ElementAt(8).ToFormattedDateTime();
                    }
                    if (examPerformedDate.HasValue)
                    {
                        //DateTime? datePerformedStart = date_performed_start.Text.TryParse();
                        //DateTime? datePerformedEnd = date_performed_end.Text.TryParse();
                        if ((search.datePerformedStart.HasValue && search.datePerformedStart.Value > examPerformedDate) || (search.datePerformedEnd.HasValue && search.datePerformedEnd.Value.AddDays(1) < examPerformedDate))
                        {
                            //fileTextLines.Clear();
                            return false;
                            //continue;
                        }

                    }
                }
            }
            return true;
        }

        private void paste_bt_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result.ToString().ToUpper() == "OK" && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
            {
                foreach (string filePath in filterdPathList)
                {
                    //string destinationFileName = System.IO.Path.Combine(dialog.SelectedPath, System.IO.Path.GetFileName(new DateTime().ToLocalTime().ToString()));
                    string sourceFilePath = filePath;
                    string destinationFilePath = System.IO.Path.Combine(dialog.SelectedPath, System.IO.Path.GetFileName(filePath));
                    File.Copy(sourceFilePath, destinationFilePath, true);
                }

                System.Windows.Forms.MessageBox.Show("Files Copied: " + filterdPathList.Count(), "Message");
            }
        }

        private void export_search_setting_button_Click(object sender, RoutedEventArgs e)
        {
            setAllSearchFields();
            string filePath = getFilePathFromFileDialogBox();
            if (filePath != null)
            {
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(filePath))
                {
                    string separator = "sep =;";
                    string header = "Keywords To Be Search;File Type;Search in Segment;Search in Field;Exam Performed Date Start;Exam Performed Date End;File Received Date Start;File Received End Date";
                    string data = string.Join(",", search.keywords) + ";" + search.fileTypes?.ConvertToDelimaterSeparetedList(",") + ";" + search.segments?.ConvertToDelimaterSeparetedList(",") + ";" + search.fields?.ConvertToDelimaterSeparetedList(",") + ";" + search.datePerformedStart.ToShortDateStringExt() + ";" + search.datePerformedEnd.ToShortDateStringExt() + ";" + search.dateReceivedStart.ToShortDateStringExt() + ";" + search.dateReceivedEnd.ToShortDateStringExt();
                    file.WriteLine(separator);
                    file.WriteLine(header);
                    file.WriteLine(data);
                }
            }
        }

        private string getFilePathFromFileDialogBox()
        {
            string filePath = null;
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "HL7SearchConfig"; // Default file name
            dlg.DefaultExt = ".hl7SC"; // Default file extension
            dlg.Filter = "HL7 Search Config (*.hl7SC)|*.hl7SC"; // Filter files by extension
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                filePath = dlg.FileName;
            }
            return filePath;
        }
    }
}
