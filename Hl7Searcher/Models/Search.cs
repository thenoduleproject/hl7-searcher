﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hl7Searcher.Models
{
    public class Search
    {
        public List<string> keywords = new List<string>();
        public List<string> fileTypes = new List<string>();
        public List<string> segments = new List<string>();
        public List<string> fields = new List<string>();
        public DateTime? datePerformedStart;
        public DateTime? datePerformedEnd;
        public DateTime? dateReceivedStart;
        public DateTime? dateReceivedEnd;
        public void clearFields()
        {
            keywords.Clear();
            fileTypes.Clear();
            segments.Clear();
            fields.Clear();
            datePerformedStart = null;
            datePerformedEnd = null;
            dateReceivedStart = null;
            dateReceivedEnd = null;
        }
    }
}
