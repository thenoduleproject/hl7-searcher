﻿using System;
using System.Collections.Generic;
namespace Hl7Searcher.Helper
{
    public static class ExtensionMethods
    {
        public static DateTime? ToFormattedDateTime(this string dateTimeString)
        {
            DateTime? dateTime = null;

            if (dateTimeString.Length == 8)
            {
                dateTime = DateTime.ParseExact(dateTimeString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (dateTimeString.Length == 10)
            {
                dateTime = DateTime.ParseExact(dateTimeString, "yyyyMMddHH", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (dateTimeString.Length == 12)
            {
                dateTime = DateTime.ParseExact(dateTimeString, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (dateTimeString.Length == 14)
            {
                dateTime = DateTime.ParseExact(dateTimeString, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
            }

            return dateTime;
        }

        public static DateTime? TryParse(this string text)
        {
            DateTime date;
            return DateTime.TryParse(text, out date) ? date : (DateTime?)null;
        }

        public static string ConvertToDelimaterSeparetedList(this List<string> list, string delimater)
        {
            return string.Join(delimater, list);
        }

        public static string ToShortDateStringExt(this DateTime? date)
        {
            string result = "";
            if (date.HasValue)
            {
                result = date.Value.ToShortDateString();
            }
            return result;
        }
    }
}
